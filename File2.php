<?php

class File2
{
	private $file_path;

	public function __construct($file_path, $create = false)
	{
		if (!file_exists($file_path) && (false === $create || false === fopen($file_path, "w"))) {
			throw new Exception("File not exists and can not be created:  {$create} , {$file_path}");
		}
		$this->file_path = $file_path;
	}

	public function getFileName()
	{
		return basename($this->file_path);  // получение имени файла, отбрасівается путь
	}

	public function getSize()
	{
		return $this->prettifySize(filesize($this->file_path)); // полцчение размера файла
	}

	public function getIsReadable()
	{
		return is_readable($this->file_path); // Определяет существование файла и доступен ли он для чтения
	}

	public function getIsWritable()
	{
		return is_writable($this->file_path);
	}

	public function getCreatedDate($format = 'Y-m-d H:i:s')
	{
		return date($format, filectime($this->file_path)); // дата создания файла
	}

	public function getLastEditedDate($format = 'Y-m-d H:i:s')
	{
		return date($format, filemtime($this->file_path)); // дата последнего изменения
	}

	private function prettifySize($bytes) // преобразования размера файла в байтах в другие величины
	{
		switch ($bytes) {
			case $bytes >= 1073741824:
				$size = number_format($bytes / 1073741824) . ' GB';
				break;
			case $bytes >= 1048576:
				$size = number_format($bytes / 1048576, 2) . ' MB';
				break;
			case $bytes >= 1024:
				$size = number_format($bytes / 1024, 2) . ' kB';
				break;
			case $bytes > 1:
				$size = $bytes . ' bytes';
				break;
			case $bytes == 1:
				$size = '1 byte';
				break;
			default:
				$size = '0 bytes';
		}
		return $size;
	}

/*public function get_content()
{
	$handler = fopen($this->file_path, 'r');
	$content = "";
	while (!feof($handler)) {
		$content .= fgets($handler);
	}
	fclose($handler);
	return $content;
}

	public function put_content () {
		$f = fopen('test.txt', 'a');
		for ($i=0; $i<50; $i++) {
			fwrite($f,Strings::get_random_string(100));
				}
		fclose($f);
		$f2 = fopen('test2.txt', 'w');
		if (file_exists('test2.txt')) {

			fwrite($f2,'
			<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet delectus, esse illo laboriosam
				molestiae nobis perspiciatis quae quas qui quia repellendus sapiente tempora ullam vero vitae. Dicta,
				dolorum!
			</div>
			<div>Culpa facere, maiores. Alias, assumenda debitis dignissimos enim et, expedita fuga ipsam officia
				perferendis quaerat quisquam reiciendis sed similique! Consequuntur dolorum eaque et impedit iste
				repellat, voluptas? Aspernatur, ex illum.
			</div>
			<div>Accusamus accusantium aspernatur dolore dolores exercitationem facere illum incidunt, itaque magni
				molestias mollitia quasi qui voluptas! Adipisci aspernatur commodi error et eveniet fugiat ipsam minus
				modi nobis sed! Debitis, officiis?
			</div>');
		} else { throw new Exception('File not exists in File2'); }
		fclose($f2);
	}*/

	public function read()
	{
		return file_get_contents($this->file_path);
	}

	/**
	 * @param string $string
	 */
	public function write($string)
	{
		file_put_contents($this->file_path, $string, FILE_APPEND);
	}
}