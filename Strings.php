<?php

class Strings
{
public static function get_random_string($length) {
	$string = '';
	for ($i=0; $i<$length; $i++) {
		$string .= chr(mt_rand(32,126));
	}
	$string .= PHP_EOL;
	return $string;
}
}